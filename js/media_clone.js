(function ($) {

    /* Ideally we would create a module that extends the media.browser.js files, however there is currently
     * no mechanism in those methods to return mutiple files. In order to impliment those extensions we would
     * have to override many of the media modules core javascript methods, making this actually less flexible
     * and more obtrusive to other modules.
     * @todo: find a better way to transport multiple files out of the iFrame
     */
    $(document).ready(function(){
        var $cloneWindow = $("#media-tab-media_clone"),
            $resultsContainer = $cloneWindow.find(".media_clone_results"),
            $results = $resultsContainer.find(".result"),
            $search = $cloneWindow.find(".media-clone-search"),
            $showAgain = $resultsContainer.find('a'),
            $cloneTop = $cloneWindow.find(".media_clone_top"),
            $mainWindowDOM = $(parent.window.document),
            mainWindow = parent.window,
            no_clone_message = "Node media items were selected to be cloned";
            
            mainWindow.Drupal.media.mediaClone.clonableMedia = [];

        // yup, we are hijacking the submit button
        $cloneWindow.find("a.fake-ok").replaceWith("<a class='button media_clone_yes' href='#'>Submit</a>"); 
        $resultsContainer.hide();

        /* re-create the closing effect */
        $cloneWindow.find(".media_clone_yes").click(function(e){
            e.preventDefault();
            if(mainWindow.Drupal.media.mediaClone.clonableMedia.length > 0){
                $mainWindowDOM.find('#field_media_clone_quantity').val(mainWindow.Drupal.media.mediaClone.clonableMedia.length);
                // we have to access the original jQuery instance on the parent page:
                mainWindow.jQuery(function($){
                    $(".field-media-clone").mousedown(); // trigger the ajax!
                    $(".ui-dialog").dialog("destroy").remove();
                    $("iframe").remove();
                });
            }else{
                alert(no_clone_message);
                return false;
            }
        });


        $search.keypress(function(e){
            if(e.which == 13){
                e.preventDefault();
                $cloneTop.hide();
                $resultsContainer.show();

                /* if someone knows of a better way to fire this ajax call after the field has been populated
                please let me know, setTimeout is ugly, but necessary */
                setTimeout(function(){
                    $.ajax({
                        type: "get",
                        url: Drupal.settings.basePath + "media_clone/objects/" + $search.val(),
                        success: function(data){
                            var output = "<ul class='clearfix'>";
                            $.each(data,function(){
                                output += "<li>" + this.preview + "</li>";
                                mainWindow.Drupal.media.mediaClone.clonableMedia.push(this);
                            });
                            output += "</ul>";
                            $results.html(output);
                        },
                    });
                }, 100);
            }
        });

        $showAgain.bind("click",function(e){
            mainWindow.Drupal.media.mediaClone.clonableMedia = [];
            e.preventDefault();
            $resultsContainer.hide();
            $search.val("");
            $cloneTop.show();
            $results.html("");
        });

    });

})(jQuery);

