(function ($) {
    namespace('Drupal.media.mediaClone');

    // Ideally, more stuff would live in this namespace
    Drupal.media.mediaClone = {
        clonableMedia:[], // cloneable media being returned, recycled per opening
    }

    // easy-peasy way to detect those completed ajax requests
    $(document).ajaxComplete(function(e, xhr, settings){
        if(settings.extraData._triggering_element_value == "Multiple Clone"){
            //annoyingly, we have to clear out each element for :empty to work
            $.each($("div.preview.launcher"),function(){
                if($(this).find(".media-item").length <= 0){
                    $(this).html("");
                }
            });

            // new party trick! here we only insert these into empty containers
            $emptyMediaContainers = $("div.preview.launcher:empty");

            // No lets run through the results, and place them into their containers
            $.each(Drupal.media.mediaClone.clonableMedia,function(index){
                $emptyMediaContainers.eq(index).html(this.preview).parent().find(".remove").show().end().parent().find("input.fid").val(this.fid);
            });
        }
    });

})(jQuery);
